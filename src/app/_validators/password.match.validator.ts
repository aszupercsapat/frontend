import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator, ValidatorFn} from '@angular/forms';

export function passwordMachValidator(passRe: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    if (typeof control.value == 'undefined' || control.value == '' || control.value == null) {
      return null;
    }

    const name = control.value;
    return name != passRe ? {'match': true} : null;
  };
}

@Directive({
  selector: '[passwordMatch]',
  providers: [{provide: NG_VALIDATORS, useExisting: PasswordMatchValidator, multi: true}]
})
export class PasswordMatchValidator implements Validator {
  @Input('passwordMatch') password: string;

  validate(control: AbstractControl): { [key: string]: any } | null {
    const passRe = control.parent.value[this.password];
    return passRe ? passwordMachValidator(passRe)(control) : null;
  }
}

