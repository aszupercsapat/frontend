import {Directive, forwardRef} from '@angular/core';
import {AbstractControl, NG_ASYNC_VALIDATORS, Validator, ValidatorFn} from '@angular/forms';
import {CityService} from "../_services";
import {Observable} from "rxjs/Rx";

@Directive({
  selector: '[postalCode]',
  providers: [{provide: NG_ASYNC_VALIDATORS, useExisting: forwardRef(() => PostalCodeValidator), multi: true}]
})
export class PostalCodeValidator implements Validator {

  constructor(private cityService: CityService) {
  }

  validate(control: AbstractControl): Promise<{ [key: string]: any }> | Observable<{ [key: string]: any }> {
    if (typeof control.value == 'undefined' || control.value == '' || control.value == null) {
      return null;
    }
    return this.validatePostalCode(control.value, this.cityService).first();
  }

  validatePostalCode(postalCode: string, cityService: CityService) {
    return new Observable(observer => {
      cityService.getByPostalCode(postalCode).subscribe(result => {
        if (result.length > 0) {
          observer.next(null);
        } else {
          observer.next({invalid: true});
        }
      }, error => {
        observer.next({invalid: true});
      });
    });
  }
}
