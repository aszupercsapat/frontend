import {Directive} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator, ValidatorFn} from '@angular/forms';

export function phoneValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (typeof control.value == 'undefined' || control.value == '' || control.value == null) {
      return null;
    }
    const pattern = '^(?:\\+36|06)(?:-|\\(|\\s)?(\\d{1,2})(?:-|\\)|\\s|\\/)?(\\d{3})(?:-|\\s)?(\\d{3,4})$';
    const phoneReg = new RegExp(pattern);
    const forbidden = !phoneReg.test(control.value);
    return forbidden ? {'phone': {value: control.value}} : null;
  };
}

@Directive({
  selector: '[phone]',
  providers: [{provide: NG_VALIDATORS, useExisting: PhoneValidator, multi: true}]
})
export class PhoneValidator implements Validator {
  validate(control: AbstractControl): { [key: string]: any } | null {
    return phoneValidator()(control) || null;
  }
}
