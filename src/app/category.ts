export class Category {
    id: number;
    name: string;
    subCategories: Subcategory[];
}

export class Subcategory {
    id: number;
    name: string;
}