import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category, Subcategory } from './category';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  //private baseUrl = 'http://localhost:3000';
   private baseUrl = 'http://127.0.0.1:8080/WebshopForPets-postgres/webshop';

  getCategories(): Observable<Category[]>{
    this.http.get<Category[]>(this.baseUrl + '/categories').subscribe(result=> console.log(result));
    return this.http.get<Category[]>(this.baseUrl + '/categories', {responseType: 'json'});
  }

  // TODO: try with backend. not used yet.
  getSubcategories(id: number): Observable<Subcategory[]> {
    return this.http.
      get<Subcategory[]>(this.baseUrl + '/categories/' + id + '/subcategories', { responseType: 'json' });
  }
}
