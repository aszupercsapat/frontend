import {Component, ElementRef, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {Router} from "@angular/router";
import {AlertService, CityService, UserService} from "../_services";
import {User, UserData, UserAddress, City} from "../_models";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  model: User = new User();
  loading = false;
  countries: any;
  typesOfPublicPlace: any;
  other: any = {};
  cityName: string;

  @ViewChild('f') regForm: any;
  @ViewChild('closeModal') public closeModal: ElementRef;


  constructor(private router: Router,
              private userService: UserService,
              private alertService: AlertService,
              private cityService: CityService) {
    this.model.userData = new UserData();
    this.model.userData.address = new UserAddress();
  }

  register() {
    if (this.regForm.valid) {
      this.registration();
    } else {
      console.error("Form is invalid!");
      console.log(this.regForm.form.controls);
    }
  }

  registration() {
    this.loading = true;
    this.userService.create(this.model).subscribe(this.success.bind(this), this.error.bind(this));
  }

  success(data) {
    console.log(data);
    this.alertService.success('Registration successful', true);
    this.loading = false;
    this.closeModal.nativeElement.click();
  }

  error(error) {
    console.error(error.message);
    this.handleErrorStatus(error.status);
    this.alertService.error(error);
    this.loading = false;
  }

  handleErrorStatus(status) {
    switch (status) {
      case 500:
        console.error('This user already exist');
        this.regForm.form.controls.emailAddress.setErrors({'exist': true});
        break;
      case 406:
        console.error(status);
        break;
    }
  }

  searchCity(code) {
    this.cityService.getByPostalCode(code).subscribe(result => {
      for (let i = 0; i < result.length; i++) {
        if(typeof result[i].address.city != 'undefined'){
          this.cityName = result[i].address.city || null;
          console.log(this.cityName);
        }
      }

    });
  }

  ngOnInit() {
    this.countries = [
      {id: 'HUNGARY', text: 'Magyarország'}
    ];

    this.typesOfPublicPlace = [
      {id: 'út', text: 'út'},
      {id: 'utca', text: 'utca'}
    ]
  }

}
