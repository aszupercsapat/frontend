import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { ProductService } from '../product.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  constructor(private productService: ProductService,
    private route: ActivatedRoute,
    private location: Location) { }

  products: Product[];
  cat_id: number;
  sub_id: number;

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.cat_id = +params['cat_id'];
        this.sub_id = +params['sub_id'];
        // console.log(this.cat_id + " " + this.sub_id);
        this.getProductsbyCatSubcat(this.cat_id, this.sub_id);
      }
    )
  }

  getProducts() {
    this.productService.getProducts().subscribe(result => { this.products = result; })
  }
  getProductsbyCatSubcat(cat_id: number, sub_id: number) {
    this.productService.getProductsbyCatSubcat(cat_id, sub_id, 0, 20).subscribe(result => { this.products = result; });
  }
}