import { Component, Input } from '@angular/core';
import { Product } from '../product';
import { CartService } from '../cart.service';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent {

  @Input() product: Product;
  constructor (private cart: CartService, private router: Router) {}

  goToDetailPage(id: number) {
    this.router.navigate(['/product', id]);
  }

}
