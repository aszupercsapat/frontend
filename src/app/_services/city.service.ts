import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {City} from "../_models";

@Injectable({
  providedIn: 'root'
})
export class CityService {
  private baseUrl = 'https://nominatim.openstreetmap.org/search?country=HU&format=json&addressdetails=1'

  constructor(private http: HttpClient) {
  }

  getByPostalCode(code: string) {
    const url = this.baseUrl + "&postalcode=" + code;
    return this.http.get<City[]>(url, {responseType: 'json'});
  }
}
