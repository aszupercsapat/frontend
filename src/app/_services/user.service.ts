import {Injectable, Inject} from '@angular/core';

import {User} from '../_models';
import {HttpClient} from "@angular/common/http";
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ReplaySubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  storageKey = 'userId';
  userId: number = 0;
  private userSubject = new ReplaySubject<number>();
  constructor(private http: HttpClient, 
    @Inject(SESSION_STORAGE) private storage: StorageService,) {
      this.userId = this.storage.get(this.storageKey);
      this.userSubject.next(this.userId);
  }

   private baseUrl = 'http://127.0.0.1:8080/WebshopForPets-postgres/webshop';
  //private baseUrl = 'http://127.0.0.1:1234/WebshopForPets-postgres/webshop';

  getById(id: number) {
    this.http.get<User[]>(this.baseUrl + '/user/' + id).subscribe(result => console.log(result));
    return this.http.get<User[]>(this.baseUrl + '/user/' + id, {responseType: 'json'});
  }

  create(user: User) {
    return this.http.post(this.baseUrl + '/user/', user, {responseType: 'json'});
  }

  update(user: User) {
    return this.http.put(this.baseUrl + '/user/' + user.id, user, {responseType: 'json'});
  }

  login(user: User) {
    return this.http.post(this.baseUrl + '/userlogin/', user, {responseType: 'json'});
  }

  saveUserData(id: number) {
    this.userId = id;
    this.storage.set(this.storageKey, id);
    this.userSubject.next(this.userId);
  }

  getUserData(): Observable<number> {
    return this.userSubject.asObservable();
  }

  logout() {
    this.storage.remove(this.storageKey);
    this.userId = 0;
    this.userSubject.next(this.userId);
  }
}
