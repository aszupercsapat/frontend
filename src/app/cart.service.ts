import { Inject, Injectable, OnInit } from '@angular/core';
import { Product } from './product';
import { ProductForCart, ProductForBackend } from './product-for-cart';
import { Subject, Observable, ReplaySubject } from 'rxjs';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service'
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { UserAddress } from './_models';
import { Order } from './order';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CartService implements OnInit{

  private baseUrl = 'http://127.0.0.1:8080/WebshopForPets-postgres/webshop';

  //TODO remove 2
  orderId: number;
  order: Order = new Order();

address : UserAddress = new UserAddress();
  constructor(@Inject(SESSION_STORAGE) private storage: StorageService,
  private http: HttpClient) {
    this.products = this.storage.get(this.storageKey) || [];
    this.cartSubject.next(this.products);
    this.countItems();

  }

  private products: ProductForCart[] = [];
  numberOfItems: number = 0;
  storageKey = 'items';
  private cartSubject = new ReplaySubject<ProductForCart[]>();

  ngOnInit() {
    
  }

  addProduct(product: Product) {
    let index = this.products.findIndex((p) => p.id === product.id);
      if (index === -1) {
        let productForCart: ProductForCart;
        productForCart = this.mapToProductForCart(product);
        this.products.push(productForCart);
      }
      else {
        this.products[index].quantity = this.products[index].quantity + 1;
      }
      this.cartSubject.next(this.products);
      this.storage.set(this.storageKey, this.products);
      this.countItems();
  }

  getItems(): Observable<ProductForCart[]> {
    return this.cartSubject.asObservable();
  }

  removeProduct(product: ProductForCart) {
    let index = this.products.findIndex((p) => p.id === product.id);
    if (this.products[index].quantity === 1) {
      this.products = this.products.filter((p) => p.id !== product.id);
      this.cartSubject.next(this.products);
    }
    else {
      this.products[index].quantity = this.products[index].quantity - 1;
    }
    this.storage.set(this.storageKey, this.products);
    this.countItems();
   
  }

  private mapToProductForCart(product: Product) : ProductForCart{
   return new ProductForCart(product.id, product.productDescription.name, product.productDescription.priceInHUF)
  }

  private mapToProductForBackend(product: ProductForCart, orderId: number): ProductForBackend {
    return new ProductForBackend(product.quantity, product.id, orderId);
  }

  private countItems(){
    this.numberOfItems = 0;
    this.products.forEach((p) => this.numberOfItems = this.numberOfItems + p.quantity);
  }

  emptyCart(){
    this.products = [];
    this.storage.remove(this.storageKey);
    this.cartSubject.next(this.products);
    this.countItems();
  }

  sumPrices(): number {
    let sum = 0;
    this.products.forEach((p) => sum = sum + p.price * p.quantity);
    return sum;
  }

  sendOrderedProducts(orderId: number) {
    this.products.forEach(p=> {
      this.http.post(this.baseUrl+"/orderedproduct", this.mapToProductForBackend(p, orderId) , {responseType: 'json'})
      .subscribe();
    });
  }
  
  sendOrder(order: Order)  {
    return this.http.post<Order>(this.baseUrl + "/order", order, {responseType: 'json'})
    // .subscribe(res => { this.orderId = res.id; this.sendOrderedProducts()});
  }
 

}