import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ProductService } from '../product.service';
import { CartService } from '../cart.service';


@Component({
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private productService: ProductService,
    private cart: CartService) { }

  product: Product = new Product();
  id: number;
  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.id = +params['id'];
        this.getProduct(this.id);
      }
    );
  }

  getProduct(id: number) {
    this.productService.getProduct(id).subscribe((result) => { this.product = result; })
  }
}