import { Component, OnInit, Inject } from '@angular/core';
import { CategoryService } from '../category.service';
import { Category, Subcategory } from '../category';
import { CartService } from '../cart.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { UserService } from '../_services';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private categoryService: CategoryService,
    private cart: CartService,
    private userService: UserService) {
  }

  categories: Category[];

 userId: any;
  ngOnInit() {
    this.userService.getUserData().subscribe(res => { this.userId = res; });
    this.getCategories();
  }

  getCategories() {
    this.categoryService.getCategories()
    .subscribe((result) => { 
      this.categories = result;});
  }

  isCartEmpty() {
    return this.cart.numberOfItems === 0;
  }

  isUserLoggedIn() : boolean {
    return this.userId > 0;
  }

  logout() {
    this.userId = 0;
    this.userService.logout();
    this.cart.emptyCart();
  }
}
