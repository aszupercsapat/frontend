import { UserAddress } from "./_models";

export class Order {
    id: number = null;
    billingAddress: UserAddress;
    shippingAddress: UserAddress;
    orderedProductIds: number[] = null;
    orderDate: Date = null;

    //TODO remove 1
    userId: number = 1;
}
