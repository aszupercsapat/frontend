import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from './product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  //private baseUrl = 'http://localhost:3000';
  private baseUrl = 'http://127.0.0.1:8080/WebshopForPets-postgres/webshop';

  getProduct(id: number): Observable<Product> {
    return this.http.get<Product>(this.baseUrl + '/product/' + id, {responseType: 'json'});
  }

  // TODO add query params: start, size
  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrl + '/product', {responseType: 'json'});
    //return this.http.get<Product[]>(this.baseUrl + '/products?start=' + start + '&size=' + size, {responseType: 'json'});
  }

  getProductsbyCategory(category_id: number, start: number, size: number): Observable<Product[]> {
    return this.http
      .get<Product[]>(this.baseUrl + '/category/' + category_id + '/products?start=' + start + '&size=' + size, { responseType: 'json' });
  }

  getProductsbyCatSubcat(category_id: number, subcategory_id: number, start: number, size: number): Observable<Product[]> {
    return this.http
       .get<Product[]>(this.baseUrl + '/category/' + category_id + '/subcategories/' + subcategory_id + '/products?start=' + start + '&size=' + size, { responseType: 'json' })
      //.get<Product[]>(this.baseUrl + '/category/' + category_id + '/subcategory/' + subcategory_id + '/products', { responseType: 'json' })

  }

  getProductsbyTag(tag: string, start: number, size: number): Observable<Product[]> {
    return this.http
      .get<Product[]>(this.baseUrl + '/products;tag=' + tag + '?start=' + start + '&size=' + size);
  }
}
