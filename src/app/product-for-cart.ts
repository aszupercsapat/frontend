export class ProductForCart {
    id: number;
    name: string;
    price: number;
    quantity: number;

    constructor(_id:number, _name: string, _price: number){
        this.id = _id;
        this.name = _name;
        this.price = _price;
        this.quantity = 1;
    }
}

export class ProductForBackend {
    id: number = null;
    quantity: number;
    productId: number;
    orderId: number;
    constructor(_quantity:number, _productId:number, _orderId:number){
        this.quantity = _quantity;
        this.productId = _productId;
        this.orderId = _orderId;
    }
}