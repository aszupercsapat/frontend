import { Component, OnInit, ViewChild } from '@angular/core';
import { Order } from '../order';
import { UserAddress } from '../_models';
import { CartService } from '../cart.service';
import { UserService } from '../_services';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  order: Order = new Order();
  address: UserAddress = new UserAddress();
  countries: any;
  typesOfPublicPlace: any;

  @ViewChild('f') regForm: any;

  userId: number = 0;
  constructor(private cart: CartService, private userService: UserService) { }

  ngOnInit() {
    this.userService.getUserData().subscribe(res => { this.userId = res; });
    this.countries = [
      {id: 'HUNGARY', text: 'Magyarország'}
    ];

    this.typesOfPublicPlace = [
      {id: 'út', text: 'út'},
      {id: 'utca', text: 'utca'}
    ]
  }
  submitted = false;

  sendOrder() { 
    if (this.regForm.valid) {
      this.order.billingAddress = this.address;
      this.order.shippingAddress = this.address;
      this.order.userId = this.userId;
      this.send();
    } else {
      console.error("Form is invalid!");
    }
    
  }
orderId: number;
  send() {
    this.cart.sendOrder(this.order).subscribe((res=> {this.orderId = res.id; this.sendProducts()}), this.error.bind(this));
  }

sendProducts() {
  this.cart.sendOrderedProducts(this.orderId);
  this.submitted = true;
  this.cart.emptyCart();
}

  error(error) {
    console.error(error.message);
    this.submitted = false;
  }

}