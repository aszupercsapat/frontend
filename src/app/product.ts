import { Category, Subcategory } from "./category";

export class Product {
    id : number;
    //image: string;
    category: Category;
    subCategory: Subcategory;
    productDescription: productDescription = new productDescription();
}

export class productDescription {
    manufacturer: string;
    name: string;
    priceInHUF: number;
    shortDescription: string;
}
