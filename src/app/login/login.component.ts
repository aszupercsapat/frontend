import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { User } from '../_models';
import { UserService, AlertService } from '../_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: User = new User();
  loading = false;
  @ViewChild('f') regForm: any;
  @ViewChild('closeModal') public closeModal: ElementRef;
  constructor(private userService: UserService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.model.id =null;
    this.model.rights = null;
    this.model.transientPassword = null;
    this.model.userData = null;
  }

  login() {
    if (this.regForm.valid) {
      this.sendLogin();
    } else {
      console.error("Form is invalid!");
    }
  }

  sendLogin() {
    this.loading = true;
     this.userService.login(this.model).subscribe(this.success.bind(this), this.error.bind(this));
    // this.userService.login(this.model)
    // .subscribe(resp=>{console.log(resp)});
  }

  success(data) {
    console.log(data);
    this.alertService.success('Login successful', true);
    this.loading = false;
    this.closeModal.nativeElement.click();
    this.userService.saveUserData(data.id);
  }

  error(error) {
    console.error(error.message);
    this.handleErrorStatus(error.status);
    this.alertService.error(error);
    this.loading = false;
  }

  handleErrorStatus(status) {
    switch (status) {
      case 500:
        console.error('Internal server error');
        // this.regForm.form.controls.emailAddress.setErrors({'exist': true});
        break;
      case 401:
        console.error(status);
        this.regForm.form.controls.emailAddress.setErrors({'exist': false});
        this.regForm.form.controls.emailAddress.transientPassword({'exist': false});
        break;
    }
  }

  log(f) {
    console.log(f);
  }
}
