import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatTableModule,
  MatBadgeModule,
  MatCardModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {LayoutModule} from '@angular/cdk/layout';
import {OverlayModule} from '@angular/cdk/overlay';
import {NavbarComponent} from './navbar/navbar.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ProductComponent} from './product/product.component';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductDetailsComponent} from './product-details/product-details.component';
import {AppRoutingModule} from './app-routing.module';
import {CartComponent} from './cart/cart.component';
import {OrderComponent} from './order/order.component';
import {StorageServiceModule} from 'ngx-webstorage-service';
import {FormsModule} from "@angular/forms";
import {AlertService, UserService, CityService} from './_services';
import {NgxSelectModule} from 'ngx-select-ex';
import {ModalModule} from "ngx-bootstrap";
import {CommonModule} from '@angular/common';
import {PasswordMatchValidator, PhoneValidator, PostalCodeValidator} from './_validators';

@NgModule({
  declarations: [
    PasswordMatchValidator,
    PhoneValidator,
    PostalCodeValidator,
    AppComponent,
    HomeComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    ProductComponent,
    ProductListComponent,
    ProductDetailsComponent,
    CartComponent,
    OrderComponent,
  ],
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    NgxSelectModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    LayoutModule,
    OverlayModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    AppRoutingModule,
    MatBadgeModule,
    MatTableModule,
    MatCheckboxModule,
    StorageServiceModule,
  ],
  exports: [
    MatBadgeModule,
    MatTableModule,
    ModalModule
  ],
  providers: [AlertService, UserService, CityService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
