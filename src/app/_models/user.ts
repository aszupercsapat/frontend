import {UserData} from "./user.data";

export class User {
  id: number = null;
  emailAddress: string;
  hashedPassword: string = null;
  rights: string = 'CUSTOMER';
  transientPassword: string;
  userData: UserData;
}
