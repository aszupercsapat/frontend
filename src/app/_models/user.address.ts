export class UserAddress {
  postalCode: number;
  countryEnum: string;
  city: string;
  nameOfPublicPlace: string;
  typeOfPublicPlace: string;
  streetNumber: string;
  floorNumber: number = null;
  door: string = null;
}
