import {UserAddress} from "./user.address";

export class UserData {
  lastName: string;
  firstName: string;
  phoneNumber: string;
  address: UserAddress;
}
