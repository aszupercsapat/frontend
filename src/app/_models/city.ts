export class City {
  place_id: string;
  licence: string;
  boundingbox: string[];
  lat: string;
  lon: string;
  display_name: string;
  class: string;
  type: string;
  importance: number;
  address: {
    city: string,
    region: string,
    postcode: string,
    country: string
    country_code: string
  };
}
