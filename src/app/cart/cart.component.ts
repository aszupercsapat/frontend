import { Component, OnInit, OnChanges } from '@angular/core';
import { CartService } from '../cart.service';
import { MatTableDataSource } from '@angular/material';
import { ProductForCart } from '../product-for-cart';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  displayedColumns: string[] = ['name', 'price', 'quantity', 'remove'];
  dataSource = new MatTableDataSource<ProductForCart>();
  isOrderVisible = false;
  
  constructor(private cart: CartService) {}

  ngOnInit() {
    this.cart.getItems().subscribe((result) => {this.dataSource.data = result;});
  }

  showOrder() {
    this.isOrderVisible = true;
    // this.cart.sendOrder();
  }
}
